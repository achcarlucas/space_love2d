debug = true

-- Player Game Object
player = {
	isAlive = nil,
	score = nil,
	x = nil, 
	y = nil, 
	speed = nil, 
	img = nil,
	init = function(self, properShoot, properEnemy)
		self.img = love.graphics.newImage('assets/sprites/plane.png')
		self.y = love.graphics.getHeight() - self.img:getHeight()
		self.speed = 150
		self.x = (love.graphics.getWidth() / 2) - (self.img:getWidth() / 2)
		-- reset player
		self:reset()
	end,
	update = function(self, dt)
		if(self.isAlive) then
			if(love.keyboard.isDown('left', 'a')) then
				if(self.x > 0) then
					self.x = self.x - (self.speed*dt)
				end
			elseif(love.keyboard.isDown('right', 'd')) then
				if(self.x < (love.graphics.getWidth() - self.img:getWidth())) then
					self.x = self.x + (self.speed*dt)
				end
			end
		else
			if not self.isAlive and love.keyboard.isDown('r') then
				properShoot:reset()
				properEnemy:reset()

				self.y = love.graphics.getHeight() - self.img:getHeight()
				self.x = (love.graphics.getWidth() / 2) - (self.img:getWidth() / 2)
				self.score = 0
				self.isAlive = true

			end
		end
	end,
	draw = function(self, dt)
		if(self.isAlive) then
			love.graphics.draw(self.img, self.x, self.y)
		else
			love.graphics.print("Press 'R' to restart", love.graphics:getWidth()/2-50, love.graphics:getHeight()/2-10)
		end
		love.graphics.print("Score: " .. self.score, 10, 10)
	end,
	reset = function(self)
		self.score = 0
		self.isAlive = true
	end
}

-- Shoot Game Object
properShoot = {
	canShoot = nil, 
	canShootTimerMax = nil, 
	canShootTimer = nil, 
	bullets = {}, 
	bulletImg = nil,
	init = function(self)
		self.canShoot = true
		self.canShootTimerMax = 0.2
		self.canShootTimer = self.canShootTimerMax
		properShoot.bulletImg = love.graphics.newImage('assets/sprites/bullet.png')
	end,
	update = function(self, player, dt)
		self.canShootTimer = self.canShootTimer - (1 * dt)
		if(self.canShootTimer < 0) then
			self.canShoot = true
		end

		for i, bullet in ipairs(self.bullets) do
			bullet.y = bullet.y - (250 * dt)
			if bullet.y < 0 then
				table.remove(self.bullets, i)
			end
		end

		if(love.keyboard.isDown(' ', 'rctrl', 'lctrl', 'ctrl') and self.canShoot) then
			newBullet = {x = player.x + (player.img:getWidth()/2), y = player.y, img = properShoot.bulletImg}
			table.insert(self.bullets, newBullet)
			self.canShoot = false
			self.canShootTimer = self.canShootTimerMax
		end
	end,
	draw = function(self, dt)
		for i, bullet in ipairs(self.bullets) do
			love.graphics.draw(bullet.img, bullet.x, bullet.y)
		end
	end,
	reset = function(self)
		bullets = {}
		self.canShoot = true
		self.canShootTimer = self.canShootTimerMax
	end
}

properEnemy = {
	createEnemyTimerMax = nil,
	createEnemyTimer = nil,
	enemyImg = nil,
	enemies = {},
	init = function(self)
		self.createEnemyTimerMax = 0.4
		self.createEnemyTimer = self.createEnemyTimerMax
		self.enemyImg = love.graphics.newImage('assets/sprites/enemy.png')
	end,
	update = function(self, dt)
		-- Time out enemy creation
		self.createEnemyTimer = self.createEnemyTimer - (1 * dt)
		if(self.createEnemyTimer < 0) then
			self.createEnemyTimer = self.createEnemyTimerMax

			-- Create an enemy
			randomNumber = math.random(10, love.graphics.getWidth() - 10)
			newEnemy = {x = randomNumber, y = -1 * self.enemyImg:getHeight(), img = self.enemyImg}
			table.insert(self.enemies, newEnemy)
		end

		for i, enemy in ipairs(self.enemies) do
			enemy.y = enemy.y + (200 * dt)

			-- Get out of screen, remove it
			if(enemy.y > love.graphics.getHeight() + enemy.img:getHeight()) then
				table.remove(self.enemies, i)
			end
		end
	end,
	draw = function(self, dt)
		for i, enemy in ipairs(self.enemies) do
			love.graphics.draw(enemy.img, enemy.x, enemy.y)
		end
	end,
	reset = function(self)
		self.createEnemyTimer = self.createEnemyTimerMax
		self.enemies = {}
	end
}

love.gamePhysics = {
	checkCollider = function(x1, y1, w1, h1, x2, y2, w2, h2)
		return 	x1 < x2+w2 and
         		x2 < x1+w1 and
         		y1 < y2+h2 and
         		y2 < y1+h1
	end
}

function love.load(arg)
	-- Init Player Object
	player:init()

	-- Init Shoot Object
	properShoot:init()

	-- Init Enemy Object
	properEnemy:init()
end

function love.update(dt)
	-- I always start with an easy way to exit the game
	if(love.keyboard.isDown('escape')) then
		love.event.push('quit')
	end

	-- Player update
	player:update(dt)

	-- Shoot update
	properShoot:update(player, dt)

	-- Enemy update
	properEnemy:update(dt)

	-- Collider player with enemy and enemy with bullets and destroy objects
	for i, enemy in ipairs(properEnemy.enemies) do
		for j, bullet in ipairs(properShoot.bullets) do
			if(love.gamePhysics.checkCollider(enemy.x, enemy.y, enemy.img:getWidth(), enemy.img:getHeight(), bullet.x, bullet.y, bullet.img:getWidth(), bullet.img:getHeight())) then
				table.remove(properShoot.bullets, j)
				table.remove(properEnemy.enemies, i)
				player.score = player.score + 1
			end
		end

		if(love.gamePhysics.checkCollider(enemy.x, enemy.y, enemy.img:getWidth(), enemy.img:getHeight(), player.x, player.y, player.img:getWidth(), player.img:getHeight()) and player.isAlive) then
			table.remove(properEnemy.enemies, i)
			player.isAlive = false
		end
	end
end

function love.draw(dt)
	-- Draw the player into the screen
	player:draw(dt)

	-- Draw shoots into the screen
	properShoot:draw(dt)

	-- Draw enemies into the screen
	properEnemy:draw(dt)
end