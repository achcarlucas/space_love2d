-- Game Configure
function love.conf(t)
	t.title 		= "Scrolling Shooter Tutorial"	-- The title of the window the game is in (string)
	t.version 		= "0.10.1"						-- The Love version this game was made for (string)
	t.window.width 	= 480							-- We want our game to be long and thin
	t.window.height = 600
	
	-- For window debugging
	t.console = true
end